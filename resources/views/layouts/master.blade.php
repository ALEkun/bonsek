<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>bonsek</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <link rel="stylesheet" href="{{ asset('css/home.css') }}">
  <link rel="stylesheet" href="{{ asset('css/footer.css') }}">

</head>
<body>
  @yield('content')




  <section class="section-find-more-blog">
    <div class="section-title-cont">
      <h2 class="title">Find more about our Blog</h2>
      <p class="subtitle">Find more about incontinence topic, tips for caregivers, advice and much more.</p>
    </div>
    <div class="cards-toblog-cont">

      <div class="card-toblog">
        <div class="img-cont">
          <img src="https://via.placeholder.com/270x200" alt="">
        </div>
        <div class="tag-cont">
          <a href="#" class="tag tag-purple">HEALTH AND WELLBEING</a>
        </div>
        <div class="info-cont">
          <h3>Myths about incontinence</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam sapiente, necessitatibus voluptatum saepe repudiandae aliquid fugiat laudantium cupiditate quasi rem, cumque in ex repellat quaerat, ea nobis natus esse dignissimos. <a href="#">Read more...</a></p>
          <a>By <strong>Bonsek editorial team</strong></a>
        </div>
      </div>

      <div class="card-toblog">
        <div class="img-cont">
          <img src="https://via.placeholder.com/270x200" alt="">
        </div>
        <div class="tag-cont">
          <a href="#" class="tag tag-orange">BONSEK MEN</a>
        </div>
        <div class="info-cont">
          <h3>What is up with Alzheimer?</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam sapiente, necessitatibus voluptatum saepe repudiandae aliquid fugiat laudantium cupiditate quasi rem, cumque in ex repellat quaerat, ea nobis na <a href="#">Read more...</a></p>
          <a>By <strong>Bonsek editorial team</strong></a>
        </div>
      </div>

      <div class="card-toblog">
        <div class="img-cont">
          <img src="https://via.placeholder.com/270x200" alt="">
        </div>
        <div class="tag-cont">
          <a href="#" class="tag tag-blue">CAREGIVERS</a>
        </div>
        <div class="info-cont">
          <h3>Isolation by incontinence</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam sapiente, necessitatibus voluptatum saepe repudiandae aliquid fugiat laudantium <a href="#">Read more...</a></p>
          <a>By <strong>Bonsek editorial team</strong></a>
        </div>
      </div>

    </div>
    
  </section>
    

  <div class="section-footer">
    <footer class="footer
    ">

    <div class="sign-up-sec">
      <div class="msj-ico-cont">
        <i class="medium material-icons">email</i>
        <strong>Sign up for our Newsletter!</strong>
      </div>
      <div class="form-cont">
        <form action="#">
          <div class="inputs-cont">
            <input class="email" type="email" placeholder="Enter Email">
            <input class="submit" type="submit" value="Suscribeme">
          </div>
        </form>
      </div>
    </div>
    <div class="links-sec">
      <div class="contactus-cont">
        <strong>CONTACT US</strong>
        <ul>
          <li> 
            <i class="material-icons">place</i> 
            <span>
              Transformación 4, Industrial Cuamatla<br>
              54730 Cuautitlán Izcalli, Méx.
            </span>
          </li>
          <li> 
            <i class="material-icons">phone</i> 
            <span>
              01 (55) 5899 7990
            </span>
          </li>
          <li> 
            <i class="material-icons">email</i> 
            <span>
              <a href="mailto:support@bonsek.com" class="dropdown">support@bonsek.com</a>
            </span>
          </li>
        </ul>
      </div>

      <div class="followus-cont">
        <strong>FOLLOW US</strong>
        <ul >
          <li><a href="" ><img src="http://via.placeholder.com/30" alt=""></a></li>
          <li><a href=""><img src="http://via.placeholder.com/30" alt=""></a></li>
          <li><a href=""><img src="http://via.placeholder.com/30" alt=""></a></li>
        </ul>
      </div>
      <div class="about-cont">
        <strong>ABOUT</strong>
        <ul>
          <li><a href=""><i class="material-icons">chevron_right</i>Our Story</a></li>
          <li><a href=""><i class="material-icons">chevron_right</i>Products</a></li>
          <li><a href=""><i class="material-icons">chevron_right</i>Incontinence Help</a></li>
          <li><a href=""><i class="material-icons">chevron_right</i>FAQ</a></li>
          <li><a href=""><i class="material-icons">chevron_right</i>Blog</a></li>
        </ul>
      </div>

      <div class="legaladvice-cont">
        <strong>LEGAL ADVICE</strong>
        <ul>
          <li><a href=""> <i class="material-icons">chevron_right</i>Terms and Conditions</a></li>
          <li><a href=""> <i class="material-icons">chevron_right</i>Privacy Advice</a></li>
          <li><a href=""> <i class="material-icons">chevron_right</i>Cookies Policies</a></li>
        </ul>
      </div>

    </div>
    <div class="copyr-sec">
      <div class="categories-cont">
        <a href="#">Bonsek Men</a href="#">
        <a href="#">Bonsek Lady</a href="#">
        <a href="#">Buy at Amazon</a href="#">
        <a href="#">Bonsek MX</a href="#">
      </div>

      <div class="logo-cont">
        <div><img src="https://via.placeholder.com/150x70" alt=""></div>
      </div>

      <div class="text-cont">
        <small>© 2019 - Artículos Higiénicos, S.A. De C.V</small>
      </div>

    </div>
  </footer>
</div>
<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
</body>
</html>


